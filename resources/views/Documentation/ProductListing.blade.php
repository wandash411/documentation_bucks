<div class="panel panel-primary">
  <div class="panel-body" style="text-align: justify; word-spacing: 5px;">
    1. Tambahkan terlebih dahulu <b>Product</b> yang akan di perjual-belikan <br>
    <p><img src="{{asset('storage/Documentation/produk1.png')}}" alt="inventory" style="margin-left: auto; margin-right: auto; display: block; padding: 15px; width: 75%;"></p>
    2. Masukan <b>Title Product/Judul Produk</b><br>
    <p><img src="{{asset('storage/Documentation/produk2.png')}}" alt="inventory" style="margin-left: auto; margin-right: auto; display: block; padding: 15px; width: 75%;"></p>
    3. Pilih <b>Brand</b> yang akan digunakan <br>
    <p><img src="{{asset('storage/Documentation/produk3.png')}}" alt="inventory" style="margin-left: auto; margin-right: auto; display: block; padding: 15px; width: 75%;"></p>
    4. Pilih <b>Gender Product</b>
    <p><img src="{{asset('storage/Documentation/produk4.png')}}" alt="inventory" style="margin-left: auto; margin-right: auto; display: block; padding: 15px; width: 75%;"></p>
    5. Pilih <b>Category Product</b><br>
    <p><img src="{{asset('storage/Documentation/produk5.png')}}" alt="inventory" style="margin-left: auto; margin-right: auto; display: block; padding: 15px; width: 75%;"></p>
    6. Pilih <b>Sub-Category Product</b><br>
    <p><img src="{{asset('storage/Documentation/produk6.png')}}" alt="inventory" style="margin-left: auto; margin-right: auto; display: block; padding: 15px; width: 75%;"></p>
    7. Isi <b>Description/Deskripsi</b> (Optional)<br>
    <p><img src="{{asset('storage/Documentation/produk7.png')}}" alt="inventory" style="margin-left: auto; margin-right: auto; display: block; padding: 15px; width: 75%;"></p>
    8. Pilih <b>Gambar</b> yang akan digunakan <br>
    <p><img src="{{asset('storage/Documentation/produk8.png')}}" alt="inventory" style="margin-left: auto; margin-right: auto; display: block; padding: 15px; width: 75%;"></p>
    9. Klik tombol <b>Simpan,</b> secara otomatis akan tersimpan dan tampil di <b>List Product/Daftar Product</b>. Untuk melihat deskripsinya klik tombol <b>+</b><br>
    <p><img src="{{asset('storage/Documentation/produk9.png')}}" alt="inventory" style="margin-left: auto; margin-right: auto; display: block; padding: 15px; width: 75%;"></p>
    <p><img src="{{asset('storage/Documentation/produk91.png')}}" alt="inventory" style="margin-left: auto; margin-right: auto; display: block; padding: 15px; width: 75%;"></p>
    10. Setelah klik tombol <b>Simpan,</b> langkah berikutnya <b>Add Item Varian To Product/Tambah Jenis Variasi Barang</b><br>
    <p><img src="{{asset('storage/Documentation/produk10.png')}}" alt="inventory" style="margin-left: auto; margin-right: auto; display: block; padding: 15px; width: 75%;"></p>
    11. Pilih <b>Color/Warna</b><br>
    <p><img src="{{asset('storage/Documentation/produk11.png')}}" alt="inventory" style="margin-left: auto; margin-right: auto; display: block; padding: 15px; width: 75%;"></p>
    12. Pilh <b>Size/Ukuran</b><br>
    <p><img src="{{asset('storage/Documentation/produk12.png')}}" alt="inventory" style="margin-left: auto; margin-right: auto; display: block; padding: 15px; width: 75%;"></p>
    13. Masukan <b>Base Price/Harga Dasar</b><br>
    <p><img src="{{asset('storage/Documentation/produk13.png')}}" alt="inventory" style="margin-left: auto; margin-right: auto; display: block; padding: 15px; width: 75%;"></p>
    14. Masukan <b>Sell Price/Harga Jual</b><br>
    <p><img src="{{asset('storage/Documentation/produk14.png')}}" alt="inventory" style="margin-left: auto; margin-right: auto; display: block; padding: 15px; width: 75%;"></p>
    15. Masukan jumlah <b>Weight/Berat</b> dari produk<br>
    <p><img src="{{asset('storage/Documentation/produk15.png')}}" alt="inventory" style="margin-left: auto; margin-right: auto; display: block; padding: 15px; width: 75%;"></p>
    16. Pilih <b>Weight Unit/Satuan Berat</b> dari produk <br>
    <p><img src="{{asset('storage/Documentation/produk16.png')}}" alt="inventory" style="margin-left: auto; margin-right: auto; display: block; padding: 15px; width: 75%;"></p>
    17. Pilih <b>Condition/Kondisi</b> dari produk <br>
    <p><img src="{{asset('storage/Documentation/produk17.png')}}" alt="inventory" style="margin-left: auto; margin-right: auto; display: block; padding: 15px; width: 75%;"></p>
    18. Pilih <b>Type/Tipe Product</b><br>
    <p><img src="{{asset('storage/Documentation/produk18.png')}}" alt="inventory" style="margin-left: auto; margin-right: auto; display: block; padding: 15px; width: 75%;"></p>
    19. Pilih <b>Image Variant/Variasi Gambar</b><br>
    <p><img src="{{asset('storage/Documentation/produk19.png')}}" alt="inventory" style="margin-left: auto; margin-right: auto; display: block; padding: 15px; width: 75%;"></p>
    20. Klik tombol <b>Simpan</b><br>
    <p><img src="{{asset('storage/Documentation/produk20.png')}}" alt="inventory" style="margin-left: auto; margin-right: auto; display: block; padding: 15px; width: 75%;"></p>
    21. Setelah <b>Add Item Variant To Product/Tambah Jenis Variasi Barang</b>, secara otomatis tersimpan dan tampil di tabel list dibawah ini. Untuk melihat lebih rinci mengenai produk klik tombol <b>+</b>
    <p><img src="{{asset('storage/Documentation/produk21.png')}}" alt="brands" style="margin-left: auto; margin-right: auto; display: block; padding: 15px; width: 75%"></p>
  </div>
</div>