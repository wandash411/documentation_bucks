<div class="panel panel-primary">
  <div class="panel-body" style="text-align: justify; word-spacing: 5px;">
    1. Tambahkan terlebih dahulu <b>Payment Method/Metode Pembayaran</b><br>
    <p><img src="{{asset('storage/Documentation/p1.png')}}" alt="payment" style="display: block; margin-left: auto; margin-right: auto; width: 75%; padding: 15px;"></p>
    2. Pilih <b>Bank</b> yang akan digunakan sebagai <b>Metode Pembayaran/Payment Method</b><br>
    <p><img src="{{asset('storage/Documentation/p2.png')}}" alt="payment" style="display: block; margin-left: auto; margin-right: auto; width: 75%; padding: 15px;"></p>
    3. Masukan <b>Nomor Rekening Perusahaan</b> yang akan digunakan <br>
    <p><img src="{{asset('storage/Documentation/p3.png')}}" alt="payment" style="display: block; margin-left: auto; margin-right: auto; width: 75%; padding: 15px;"></p>
    4. Masukan <b>Nama Pemilik Rekening Perusahaan</b><br>
    <p><img src="{{asset('storage/Documentation/p4.png')}}" alt="payment" style="display: block; margin-left: auto; margin-right: auto; width: 75%; padding: 15px;"></p>
    5. Klik tombol <b>SAVE</b><br>
    <p><img src="{{asset('storage/Documentation/p5.png')}}" alt="payment" style="display: block; margin-left: auto; margin-right: auto; width: 75%; padding: 15px;"></p>
    6. <b>Payment Method/Metode Pembayaran</b> yang telah ditambahkan, secara otomatis tersimpan dan tampil di <b>List Payment-Method/Daftar Metode Pembayaran</b><br>
    <p><img src="{{asset('storage/Documentation/p6.png')}}" alt="payment" style="display: block; margin-left: auto; margin-right: auto; width: 75%; padding: 15px;;"></p>
  </div>
</div>