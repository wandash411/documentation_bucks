<div class="panel panel-primary">
  <div class="panel-body" style="text-align: justify; word-spacing: 5px;">
    1. Pilih <b>Date/Tanggal</b> ketika melakukan <b>Purchase Order</b><br>
    <p><img src="{{asset('storage/Documentation/pr1.png')}}" alt="productreceipt" style="margin-left: auto; margin-right: auto; display: block; padding: 15px; width: 75%;"></p>
    2. Kemudian pilih <b>Purchase Order/PO</b> yang telah tersimpan pada <b>List Purchase Order</b><br>
    <p><img src="{{asset('storage/Documentation/pr2.png')}}" alt="productreceipt" style="margin-left: auto; margin-right: auto; display: block; padding: 15px; width: 75%;"></p>
    3. Isi <b>Note/Catatan</b>(Optional) <br>
    <p><img src="{{asset('storage/Documentation/pr3.png')}}" alt="productreceipt" style="margin-left: auto; margin-right: auto; display: block; padding: 15px; width: 75%;"></p>
    <ul><b>Catatan: Note/Catatan</b> boleh di isi apabila ada catatan tambahan mengenai produk, apabila tidak ada catatan tambahan mengenai produk tidak perlu di isi.</ul>
    4. Klik tombol <b>SAVE</b>
    <p><img src="{{asset('storage/Documentation/pr4.png')}}" alt="productreceipt" style="margin-left: auto; margin-right: auto; display: block; padding: 15px; width: 75%;"></p>
    5. <b>Purchase Order</b> yang sebelumnya telah tersimpan secara otomatis datanya di <b>Product Receipt</b><br>
    <p><img src="{{asset('storage/Documentation/pr5.png')}}" alt="productreceipt" style="margin-left: auto; margin-right: auto; display: block; padding: 15px; width: 75%;"></p>
    6. Klik tombol <b>New Receipt</b><br>
    <p><img src="{{asset('storage/Documentation/pr6.png')}}" alt="productreceipt" style="margin-left: auto; margin-right: auto; display: block; padding: 15px; width: 75%;"></p>
    7. Setelah klik tombol <b>New Receipt</b>, data akan tampil dan tersimpan secara otomatis dan status <b>Receipt</b> berubah <br>
    <p><img src="{{asset('storage/Documentation/pr7.png')}}" alt="productreceipt" style="margin-left: auto; margin-right: auto; display: block; padding: 15px; width: 75%;"></p>
    8. Klik tombol <b>Approve</b>
    <p><img src="{{asset('storage/Documentation/pr8.png')}}" alt="productreceipt" style="margin-left: auto; margin-right: auto; display: block; padding: 15px; width: 75%;"></p>
    9. Setelah klik tombol <b>Approve</b>, maka status <b>Receipt</b> berubah <br>
    <p><img src="{{asset('storage/Documentation/pr9.png')}}" alt="productreceipt" style="margin-left: auto; margin-right: auto; display: block; padding: 15px; width: 75%;"></p>
    10. Klik tombol <b>Approve</b><br>
    <p><img src="{{asset('storage/Documentation/pr10.png')}}" alt="productreceipt" style="margin-left: auto; margin-right: auto; display: block; padding: 15px; width: 75%;"></p>
    11. Setelah klik tombol <b>Approve</b>, secara otomatis status <b>Receipt</b> berubah <br>
    <p><img src="{{asset('storage/Documentation/pr11.png')}}" alt="productreceipt" style="margin-left: auto; margin-right: auto; display: block; padding: 15px; width: 75%;"></p>
  </div>
</div>