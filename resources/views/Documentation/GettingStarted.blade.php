<div class="panel panel-primary">
  <div class="panel-heading">BMS (Bucks Management System) Documentation</div>
  <div class="panel-body" style="text-align: justify; word-spacing: 5px;">Dokumen ini berisi referensi dan pedoman untuk layanan aplikasi BMS (Bucks Management System). Aplikasi ini dapat membantu pengguna dalam mengelola produk, pesanan dan toko dengan menggunakan fitur-fitur yang sudah tersedia pada aplikasi BMS (Bucks Management System). <br></div>
</div>
<div class="panel panel-primary">
  <div class="panel-heading">Fitur</div>
  <div class="panel-body">
    <ul class="nav nav-tabs">
      <li class="active"><a data-toggle="tab" href="#Dashboard">Dashboard</a></li>
      <li><a data-toggle="tab" href="#Master">Master</a></li>
      <li><a data-toggle="tab" href="#Preferences">Preferences</a></li>
      <li><a data-toggle="tab" href="#Inventory">Inventory</a></li>
      <li><a data-toggle="tab" href="#Sales">Sales</a></li>
      <li><a data-toggle="tab" href="#Report">Report</a></li>
      <li><a data-toggle="tab" href="#Sales">Analytic</a></li>
    </ul>
    <div class="tab-content">
      <div id="Dashboard" class="tab-pane fade in active" style="padding: 15px; text-align: justify; word-spacing: 5px;">
        <b>1. Total Sales:</b> Menginformasikan jumlah uang yang diterima perusahaan atau suatu jenis usaha dari aktivitasnya, baik itu dari penjualan produk ataupun jasa kepada pelanggannya/customer <br>
        <b>2. Product Distribution/Distribusi Produk:</b> Kegiatan pemasaran berupa penyampaian suatu produk dari perusahaan/produsen kepada konsumen melalui perantara seperti distributor <br>
        <b>3. Product Receipt:</b> Mencatat transaksi order dan invoice yang dikeluarkan setelah pelanggan membayar tunai <br>
        <b>4. Financial Balance</b><br>
        <b>5. Asset On Hand</b><br>
        <b>6. Asset On Store</b><br>
        <b>7. Product Withdrawl</b><br>
        <b>8. Total Shipping:</b> Menginformasikan jumlah biaya pengiriman produk <br>
        <b>9. Average Sales:</b> Menginformasikan rata-rata penjualan per hari dari produk yang di distribusi <br>
        <b>10. Customers:</b> Menginformasikan jumlah pengguna yang terdaftar <br>
        <b>11. Total Discount:</b> Menginformasikan jumlah potongan harga yang telah diberikan kepada pengguna br
      </div>
      <div id="Master" class="tab-pane fade" style="padding: 15px; text-align: justify; word-spacing: 5px;">
        <b>Payment :</b> Menambahkan metode pembayaran secara manual, agar sesuai dengan bank dan nomor rekening yang telah dimiliki dan digunakan oleh pemilik perusahaan
      </div>
      <div id="Preferences" class="tab-pane fade" style="padding: 15px; text-align: justify; word-spacing: 5px;">
        <b>1. Users:</b> Untuk membuat user, mengatur hak akses yang berbeda-beda, tergantung kewenangan yang diberikan<br>
        <b>2. Brands:</b> Memberi identitas pada barang atau jasa yang berfungsi menjamin kualitas suatu barang dan jasa bagi konsumen<br>
        <b>3. Vendor:</b> Sebagai pihak yang memastikan tersedianya barang atau jasa yang dibutuhkan sebuah perusahaan untuk kegiatan atau aktivitas operasionalnya<br>
        <b>4. Store:</b> Perusahaan dengan karakteristik yang besar, atau perusahaan dengan variasi produknya yang sangat banyak<br>
        <b>5. Customer:</b> Membuat ID Card yang diberikan kepada orang-orang yang telah setia menjadi klien maupun customer sebuah perusahaan<br>
      </div>
      <div id="Inventory" class="tab-pane fade" style="padding: 15px; text-align: justify; word-spacing: 5px;">
        <b>1. Product Store Return:</b> Proses pengembalian barang dari customer ke perusahaan yang berkaitan dengan dokumen sales invoice yang sudah terbentuk<br>
        <b>2. Product Listing:</b> Menyampaikan informasi produk dan jasa suatu perusahaan kepada calon konsumen dengan informasi yang padat dan jelas, berfungsi sebagai media promosi produk dan jasa suatu perusahaan kepada konsumen, berfungsi membantu untuk menekan biaya promosi atau pemasaran produk<br>
        <b>3. Product Order:</b> Meminimalisir resiko terjadinya penipuan atas pemesanan sebuah barang yang dilakukan, membantu perusahaan dalam mendapatkan keuntungan atas pembelian sebuah barang, membantu dalam mengamankan barang yang telah di pesan<br>
        <b>4. Product Receipt:</b> Mencatat transaksi order dan invoice yang dikeluarkan setelah pelanggan membayar tunai<br>
        <b>5. Product Distribution:</b> Penyampaian suatu produk dari perusahaan/produsen kepada konsumen melalui perantara distributor<br>
        <b>6. Product Withdrawl:</b><br>
        <b>7. Storage:</b> Penyimpanan barang atau produk dalam suatu gudang (Storage) diatur dan ditata sesuai dengan kebijakan perusahaan yang telah ditentukan<br>
        <b>8. Opname:</b> Kegiatan menghitung kembali stok barang yang terdapat di persediaan perusahaan, Opname dilakukan untuk memminimalisir perbedaan antara catatan fisik dengan catatan pembukuan<br>
      </div>
      <div id="Sales" class="tab-pane fade" style="padding: 15px; text-align: justify; word-spacing: 5px;">
        <b>1. POS (Point Of Sales):</b> Membantu dan memudahkan para pemilik bisnis dalam menjalankan transaksi, bisa menghitung secara cepat, mencetak invoice atau struk bagi pelanggan, tempat penyimpanan data customer, menghitung laba dan rugi, dan merekap laporan penjualan<br>
        <b>2. History:</b> Memudahkan pengguna untuk dapat melihat seluruh transaksi yang pernah pengguna lakukan<br>
      </div>
      <div id="Report" class="tab-pane fade" style="padding: 15px; text-align: justify; word-spacing: 5px;">
        <b>Consignment Sales:</b> Sebuah perjanjian di mana Anda sebagai pihak yang memiliki barang (consignor) menyerahkan sejumlah barang Anda kepada pihak tertentu (consignee) untuk dijual dengan memberikan komisi tertentu
      </div>
      <div id="Analytic" class="tab-pane fade" style="padding: 15px; text-align: justify; word-spacing: 5px;">
        <b>Sales:</b> Memperlihatkan data berupa tren penjualan perusahaan dalam durasi tertentu. Bentuk paling dasar dari laporan analisis penjualan dapat menunjukan adanya peningkatan atau penurunan angka penjualan. Memperlihatkan tren penjualan dalam kurun mingguan, bulanan, ataupun tahunan. Hal ini disesuaikan dengan kebutuhan
      </div>
    </div>
  </div>
</div>
<div class="panel panel-primary">
  <div class="panel-heading">Quick Start</div>
  <div class="panel-body" style="text-align: justify; word-spacing: 5px;">
    Pengguna dapat mencoba semua fitur-fitur yang sudah tersedia dengan mudah. Untuk penjelasan lebih lanjut tentang cara menggunakan aplikasi ini, silakan klik <a href="{{url('/admin/documentation/payment')}}">Guide</a>
  </div>
</div>

<!-- <div id="accordion">
    <div class="card">
      <div class="card-header">
        <a class="card-link" data-toggle="collapse" href="#collapseOne">
          Dashboard
        </a>
      </div>
      <div id="collapseOne" class="collapse show" data-parent="#accordion">
        <div class="card-body">
            <p><b>Total Sales:</b> Menginformasikan jumlah uang yang diterima perusahaan atau suatu jenis usaha dari aktivitasnya, baik itu dari penjualan produk ataupun jasa kepada pelanggannya/customer</p>
            <p><b>Product Distribution/Distribusi Produk:</b> Kegiatan pemasaran berupa penyampaian suatu produk dari perusahaan/produsen kepada konsumen melalui perantara seperti distributor</p>
            <p><b>Product Receipt:</b> Mencatat transaksi order dan invoice yang dikeluarkan setelah pelanggan membayar tunai</p>
            <p><b>Financial Balance</b></p>
            <p><b>Asset On Hand</b></p>
            <p><b>Asset On Store</b></p>
            <p><b>Product Withdrawl</b></p>
            <p><b>Total Shipping:</b> Menginformasikan jumlah biaya pengiriman produk</p>
            <p><b>Average Sales:</b> Menginformasikan rata-rata penjualan per hari dari produk yang di distribusi</p>
            <p><b>Customers:</b> Menginformasikan jumlah pengguna yang terdaftar</p>
            <p><b>Total Discount:</b> Menginformasikan jumlah potongan harga yang telah diberikan kepada pengguna</p>
        </div>
      </div>
    </div>
    <div class="card">
      <div class="card-header">
        <a class="collapsed card-link" data-toggle="collapse" href="#collapseTwo">
        Master
      </a>
      </div>
      <div id="collapseTwo" class="collapse" data-parent="#accordion">
        <div class="card-body">
          <b>Payment:</b> Menambahkan metode pembayaran secara manual, agar sesuai dengan bank dan nomor rekening yang telah dimiliki dan digunakan oleh pemilik perusahaan. Untuk mengetahui langkah-langkah penggunaannya silakan klik <a href="payment">di sini</a>.
        </div>
      </div>
    </div>
    <div class="card">
      <div class="card-header">
        <a class="collapsed card-link" data-toggle="collapse" href="#collapseThree">
          Preferences
        </a>
      </div>
      <div id="collapseThree" class="collapse" data-parent="#accordion">
        <div class="card-body">
          <p><b>Users:</b> Untuk membuat user, mengatur hak akses yang berbeda-beda, tergantung kewenangan yang diberikan</p>
          <p><b>Brands:</b> Memberi identitas pada barang atau jasa yang berfungsi menjamin kualitas suatu barang dan jasa bagi konsumen. Untuk mengetahui langkah-langkah penggunaannya silakan klik <a href="preferences">di sini</a>.</p>
          <p><b>Vendor:</b> Sebagai pihak yang memastikan tersedianya barang atau jasa yang dibutuhkan sebuah perusahaan untuk kegiatan atau aktivitas operasionalnya. Untuk mengetahui langkah-langkah penggunaannya silakan klik <a href="vendor">di sini</a>.</p>
          <p><b>Store:</b> Perusahaan dengan karakteristik yang besar, atau perusahaan dengan variasi produknya yang sangat banyak</p>
          <p><b>Customer:</b> Membuat ID Card yang diberikan kepada orang-orang yang telah setia menjadi klien maupun customer sebuah perusahaan</p>
        </div>
      </div>
    </div>
    <div class="card">
      <div class="card-header">
        <a class="collapsed card-link" data-toggle="collapse" href="#collapseFour">
          Inventory
        </a>
      </div>
      <div id="collapseFour" class="collapse" data-parent="#accordion">
        <div class="card-body">
          <p><b>Product Store Return:</b> Proses pengembalian barang dari customer ke perusahaan yang berkaitan dengan dokumen sales invoice yang sudah terbentuk</p>
          <p><b>Product Listing:</b> Menyampaikan informasi produk dan jasa suatu perusahaan kepada calon konsumen dengan informasi yang padat dan jelas, berfungsi sebagai media promosi produk dan jasa suatu perusahaan kepada konsumen, berfungsi membantu untuk menekan biaya promosi atau pemasaran produk. Untuk mengetahui langkah-langkah penggunaannya silakan klik <a href="productlisting">di sini</a>.</p>
          <p><b>Purchase Order:</b> Meminimalisir resiko terjadinya penipuan atas pemesanan sebuah barang yang dilakukan, membantu perusahaan dalam mendapatkan keuntungan atas pembelian sebuah barang, membantu dalam mengamankan barang yang telah di pesan. Untuk mengetahui langkah-langkah penggunaannya silakan klik <a href="purchaseorder">di sini</a>.</p>
          <p><b>Product Receipt:</b> Mencatat transaksi order dan invoice yang dikeluarkan setelah pelanggan membayar tunai. Untuk mengetahui langkah-langkah penggunaannya silakan <a href="productreceipt">di sini</a>.</p>
          <p><b>Product Distribution:</b> Penyampaian suatu produk dari perusahaan/produsen kepada konsumen melalui perantara distributor</p>
          <p><b>Product Withdrawl:</b></p>
          <p><b>Storage:</b> Penyimpanan barang atau produk dalam suatu gudang (Storage) diatur dan ditata sesuai dengan kebijakan perusahaan yang telah ditentukan</p>
          <p><b>Opname:</b> Kegiatan menghitung kembali stok barang yang terdapat di persediaan perusahaan, <b>Opname</b> dilakukan untuk memminimalisir perbedaan antara catatan fisik dengan catatan pembukuan</p>
        </div>
      </div>
    </div>
    <div class="card">
      <div class="card-header">
        <a class="collapsed card-link" data-toggle="collapse" href="#collapseFive">
          Sales
        </a>
      </div>
      <div id="collapseFive" class="collapse" data-parent="#accordion">
        <div class="card-body">
          <p><b>POS (Point Of Sales):</b> Membantu dan memudahkan para pemilik bisnis dalam menjalankan transaksi, bisa menghitung secara cepat, mencetak invoice atau struk bagi pelanggan, tempat penyimpanan data customer, menghitung laba dan rugi, dan merekap laporan penjualan. Untuk mengetahui langkah-langkah penggunaannya silakan klik <a href="pointofsales">di sini</a>.</p>
          <p><b>History:</b> Memudahkan pengguna untuk dapat melihat seluruh transaksi yang pernah pengguna lakukan. Untuk mengetahui langkah-langkahnya klik <a href="history">di sini</a></p>
        </div>
      </div>
    </div>
    <div class="card">
      <div class="card-header">
        <a class="collapsed card-link" data-toggle="collapse" href="#collapseSix">
          Report
        </a>
      </div>
      <div id="collapseSix" class="collapse" data-parent="#accordion">
        <div class="card-body">
          <p><b>Consignment Sales:</b> Sebuah perjanjian di mana Anda sebagai pihak yang memiliki barang (consignor) menyerahkan sejumlah barang  Anda kepada pihak tertentu (consignee) untuk dijual dengan memberikan komisi tertentu</p>
        </div>
      </div>
    </div>
    <div class="card">
      <div class="card-header">
        <a class="collapsed card-link" data-toggle="collapse" href="#collapseSeven">
          Analytic
        </a>
      </div>
      <div id="collapseSeven" class="collapse" data-parent="#accordion">
        <div class="card-body">
          <p><b>Sales:</b> Memperlihatkan data berupa tren penjualan perusahaan dalam durasi tertentu. Bentuk paling dasar dari laporan analisis penjualan dapat menunjukan adanya peningkatan atau penurunan angka penjualan. Memperlihatkan tren penjualan dalam kurun mingguan, bulanan, ataupun tahunan. Hal ini disesuaikan dengan kebutuhan.</p>
        </div>
      </div>
    </div>
  </div> -->

<!-- <div class="container"  style="padding: 15px;">
        <h3 style="text-align: left;">Getting Started</h3><br>
          <a href="dashboard" style="color: black;">Dashboard</a><br>
          <a href="payment" style="color: black;">Master</a><br>
          <a href="preferences" style="color: black;">Preferences</a><br>
          <a href="inventory" style="color: black;">Inventory</a><br>
          <a href="sales" style="color: black;">Sales</a><br>
          <a href="report" style="color: black;">Report</a><br>
          <a href="analytic" style="color: black;">Analytic</a><br>
    </div> -->