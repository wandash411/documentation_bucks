<div class="panel panel-primary">
    <div class="panel-body" style="text-align: justify; word-spacing: 5px;">
        1. Apabila telah melakukan <b>POS (Point Of Sales)</b>, maka secara otomatis akan tersimpan di <b>History > List History</b> baik yang <b>Direct Transaction/Shipped Transaction</b> <br>
        <p><img src="{{asset('storage/Documentation/history1.png')}}" alt="shippedtransaction" style="margin-left: auto; margin-right: auto; display: block; padding: 15px; width: 75%;"></p>
        2. Klik tombol <b>Approve</b><br>
        <p><img src="{{asset('storage/Documentation/history2.png')}}" alt="shippedtransaction" style="margin-left: auto; margin-right: auto; display: block; padding: 15px; width: 75%;"></p>
        3. Setelah klik tombol <b>Approve</b>, maka secara otomatis <b>Status</b> pada <b>List History</b> akan berubah <br>
        <p><img src="{{asset('storage/Documentation/history3.png')}}" alt="shippedtransaction" style="margin-left: auto; margin-right: auto; display: block; padding: 15px; width: 75%;"></p>
        4. Klik tombol <b>Approve</b><br>
        <p><img src="{{asset('storage/Documentation/history4.png')}}" alt="shippedtransaction" style="margin-left: auto; margin-right: auto; display: block; padding: 15px; width: 75%;"></p>
        5. Setelah klik tombol <b>Approve</b> maka <b>Status</b> pada <b>List History</b> akan berubah <br>
        <p><img src="{{asset('storage/Documentation/history5.png')}}" alt="shippedtransaction" style="margin-left: auto; margin-right: auto; display: block; padding: 15px; width: 75%;"></p>
        6. Klik tombol <b>Delivery</b><br>
        <p><img src="{{asset('storage/Documentation/history6.png')}}" alt="shippedtransaction" style="margin-left: auto; margin-right: auto; display: block; padding: 15px; width: 75%;"></p>
        7. Setelah klik tombol <b>Delivery</b> maka <b>Status</b> pada <b>List History</b> akan berubah secara otomatis <br>
        <p><img src="{{asset('storage/Documentation/history7.png')}}" alt="shippedtransaction" style="margin-left: auto; margin-right: auto; display: block; padding: 15px; width: 75%;"></p>
        8. Apabila barang yang dikirimkan telah sampai dan telah diterima oleh pemesan, klik tombol <b>Done</b><br>
        <p><img src="{{asset('storage/Documentation/history8.png')}}" alt="shippedtransaction" style="margin-left: auto; margin-right: auto; display: block; padding: 15px; width: 75%;"></p>
        9. Setelah klik tombol <b>Done,</b> maka secara otomatis <b>Status</b> pada <b>List History</b> akan berubah <br>
        <p><img src="{{asset('storage/Documentation/history9.png')}}" alt="shippedtransaction" style="margin-left: auto; margin-right: auto; display: block; padding: 15px; width: 75%;"></p>
    </div>
</div>