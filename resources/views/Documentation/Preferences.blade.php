<div class="panel panel-primary">
  <div class="panel-body" style="text-align: justify; word-spacing: 5px;">
    1. Tambahkan terlebih dahulu <b>Brands</b> yang akan digunakan <br>
    <p><img src="{{asset('storage/Documentation/b1.png')}}" alt="brands" style="margin-left: auto; margin-right: auto; display: block; padding: 15px; width: 75%;"></p>
    2. Masukan <b>Consignment/Titip Jual</b> (Optional) <br>
    <p><img src="{{asset('storage/Documentation/b2.png')}}" alt="brands" style="margin-left: auto; margin-right: auto; display: block; padding: 15px; width: 75%;"></p>
    <ul>
      <li><b>Catatan:</b> Pada bagian <b>Consignment/Titip Jual</b> boleh di isi apabila ada yang titip jual, apabila tidak ada yang titip jual tidak perlu di isi.</li>
    </ul><br>
    3. Masukan <b>Code/Code Brand</b><br>
    <p><img src="{{asset('storage/Documentation/b3.png')}}" alt="brands" style="margin-left: auto; margin-right: auto; display: block; padding: 15px; width: 75%;"></p>
    4. Masukan <b>Name/Nama Brand</b><br>
    <p><img src="{{asset('storage/Documentation/b4.png')}}" alt="brands" style="margin-left: auto; margin-right: auto; display: block; padding: 15px; width: 75%;"></p>
    5. Masukan <b>Phone/Nomor Telepon Brand</b><br>
    <p><img src="{{asset('storage/Documentation/b5.png')}}" alt="brands" style="margin-left: auto; margin-right: auto; display: block; padding: 15px; width: 75%;"></p>
    6. Masukan <b>Address/Alamat</b><b>Brand</b><br>
    <p><img src="{{asset('storage/Documentation/b6.png')}}" alt="brands" style="margin-left: auto; margin-right: auto; display: block; padding: 15px; width: 75%;"></p>
    7. Pilih <b>Photo</b> yang akan digunakan <br>
    <p><img src="{{asset('storage/Documentation/b7.png')}}" alt="brands" style="margin-left: auto; margin-right: auto; display: block; padding: 15px; width: 75%;"></p>
    8. Klik tombol <b>SAVE</b>
    <p><img src="{{asset('storage/Documentation/b8.png')}}" alt="brands" style="margin-left: auto; margin-right: auto; display: block; padding: 15px; width: 75%;"></p>
    9. <b>Brand</b> yang telah ditambahkan, secara otomatis akan tersimpan dan tampil di <b>List Brand/Daftar Brand</b>
    <p><img src="{{asset('storage/Documentation/b9.png')}}" alt="brands" style="margin-left: auto; margin-right: auto; display: block; padding: 15px; width: 75%;"></p>
  </div>
</div>

<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>

</html>