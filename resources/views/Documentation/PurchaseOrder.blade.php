<div class="panel panel-primary">
  <div class="panel-body" style="text-align: justify; word-spacing: 5px;">
    1. Tambahkan <b>Create Order</b> terlebih dahulu <br>
    <p><img src="{{asset('storage/Documentation/po1.png')}}" alt="productorder" style="margin-left: auto; margin-right: auto; display: block; padding: 15px; width: 75%;"></p>
    2. Kemudian pilih tanggal <b>Purchase Order/PO</b> yang telah tersimpan pada <b>List Purchase Order</b><br>
    <p><img src="{{asset('storage/Documentation/po2.png')}}" alt="productorder" style="margin-left: auto; margin-right: auto; display: block; padding: 15px; width: 75%;"></p>
    3. Pilih <b>Vendor</b> yang akan digunakan <br>
    <p><img src="{{asset('storage/Documentation/po3.png')}}" alt="productorder" style="margin-left: auto; margin-right: auto; display: block; padding: 15px; width: 75%;"></p>
    4. Isi <b>Note/Catatan</b> (Optional) <br>
    <p><img src="{{asset('storage/Documentation/po4.png')}}" alt="productorder" style="margin-left: auto; margin-right: auto; display: block; padding: 15px; width: 75%;"></p>
    <ul><b>Catatan:</b> <b>Note/Catatan</b> boleh di isi apabila ada catatan tambahan ketika order, apabila tidak ada catatan tambah mengenai order tidak perlu di isi.</ul>
    5. Klik tombol <b>SAVE</b><br>
    <p><img src="{{asset('storage/Documentation/po5.png')}}" alt="productorder" style="margin-left: auto; margin-right: auto; display: block; padding: 15px; width: 75%;"></p>
    6. Setelah menambahkan <b>Create Order</b>, maka akan secara otomatis tersimpan dan tampil di <b>List Order/Daftar Order</b><br>
    <p><img src="{{asset('storage/Documentation/po6.png')}}" alt="productorder" style="margin-left: auto; margin-right: auto; display: block; padding: 15px; width: 75%;"></p>
    7. Pilih <b>Product</b> yang telah di order <br>
    <p><img src="{{asset('storage/Documentation/po7.png')}}" alt="productorder" style="margin-left: auto; margin-right: auto; display: block; padding: 15px; width: 75%;"></p>
    8. Masukan jumlah <b>Quantity/Kuantitas</b> barang yang akan di pesan <br>
    <p><img src="{{asset('storage/Documentation/po8.png')}}" alt="productorder" style="margin-left: auto; margin-right: auto; display: block; padding: 15px; width: 75%;"></p>
    9. Klik tombol <b>SAVE</b> <br>
    <p><img src="{{asset('storage/Documentation/po9.png')}}" alt="productorder" style="margin-left: auto; margin-right: auto; display: block; padding: 15px; width: 75%;"></p>
    10. Setelah <b>Add Item Purchase Order</b>, secara otomatis akan tersimpan dan tampil pada tabel seperti pada gambar berikut di bawah ini. <br>
    <p><img src="{{asset('storage/Documentation/po10.png')}}" alt="productorder" style="margin-left: auto; margin-right: auto; display: block; padding: 15px; width: 75%;"></p>
    11. Setelah selesai kemudian klik tombol <b>New Order</b><br>
    <p><img src="{{asset('storage/Documentation/po11.png')}}" alt="productorder" style="margin-left: auto; margin-right: auto; display: block; padding: 15px; width: 75%;"></p>
    12. Setelah klik tombol <b>New Order</b>, <b>Status</b> order akan berubah secara otomatis <br>
    <p><img src="{{asset('storage/Documentation/po12.png')}}" alt="productorder" style="margin-left: auto; margin-right: auto; display: block; padding: 15px; width: 75%;"></p>
    13. Klik tombol <b>Approve</b><br>
    <p><img src="{{asset('storage/Documentation/po13.png')}}" alt="productorder" style="margin-left: auto; margin-right: auto; display: block; padding: 15px; width: 75%;"></p>
    14. Setelah klik tombol <b>Approve</b>, maka secara otomatis <b>Status</b> order akan berubah
    <p><img src="{{asset('storage/Documentation/po14.png')}}" alt="productorder" style="margin-left: auto; margin-right: auto; display: block; padding: 15px; width: 75%;"></p>
  </div>
</div>