<div class="panel panel-primary">
  <div class="panel-body" style="text-align: justify; word-spacing: 5px;">
    <u><b>Direct Transaction:</b></u><br>
    1. Pilih produk <br>
    <p><img src="{{asset('storage/Documentation/pos1.png')}}" alt="pointofsales" style="margin-left: auto; margin-right: auto; display: block; padding: 15px; width: 75%;"></p>
    2. Masukan jumlah produk <br>
    <p><img src="{{asset('storage/Documentation/pos2.png')}}" alt="pointofsales" style="margin-left: auto; margin-right: auto; display: block; padding: 15px; width: 75%;"></p>
    3. Pilih <b>Discount</b> yang akan digunakan (Optional)
    <p><img src="{{asset('storage/Documentation/pos3.png')}}" alt="pointofsales" style="margin-left: auto; margin-right: auto; display: block; padding: 15px; width: 75%;"></p>
    4. Klik tombol <b>Purchase</b>
    <p><img src="{{asset('storage/Documentation/pos4.png')}}" alt="pointofsales" style="margin-left: auto; margin-right: auto; display: block; padding: 15px; width: 75%;"></p>
    5. Pilih <b>Transaction Type/Tipe Transaksi</b> yang akan digunakan <b>Direct Transaction</b><br>
    <p><img src="{{asset('storage/Documentation/pos5.png')}}" alt="pointofsales" style="margin-left: auto; margin-right: auto; display: block; padding: 15px; width: 75%;"></p>
    6. Pilih <b>Discount</b> yang akan digunakan (Optional) <br>
    <p><img src="{{asset('storage/Documentation/pos6.png')}}" alt="pointofsales" style="margin-left: auto; margin-right: auto; display: block; padding: 15px; width: 75%;"></p>
    7. Masukan jumlah <b>Discount</b> yang diberikan kepada konsumen (Optional) <br>
    <p><img src="{{asset('storage/Documentation/pos7.png')}}" alt="pointofsales" style="margin-left: auto; margin-right: auto; display: block; padding: 15px; width: 75%;"></p>
    8. Pilih <b>Member</b>, apabila memiliki <b>Member Card</b> (Optional) <br>
    <p><img src="{{asset('storage/Documentation/pos8.png')}}" alt="pointofsales" style="margin-left: auto; margin-right: auto; display: block; padding: 15px; width: 75%;"></p>
    9. Pilih <b>Payment Method/Metode Pembayaran</b> yang akan digunakan ketika melakukan transaksi <br>
    <p><img src="{{asset('storage/Documentation/pos9.png')}}" alt="pointofsales" style="margin-left: auto; margin-right: auto; display: block; padding: 15px; width: 75%;"></p>
    10. <b>Note,</b> boleh di isi atau tidak di isi pun tidak apa-apa <br>
    <p><img src="{{asset('storage/Documentation/pos10.png')}}" alt="pointofsales" style="margin-left: auto; margin-right: auto; display: block; padding: 15px; width: 75%;"></p>
    11. Klik tombol <b>CheckOut</b><br>
    <p><img src="{{asset('storage/Documentation/pos11.png')}}" alt="pointofsales" style="margin-left: auto; margin-right: auto; display: block; padding: 15px; width: 75%;"></p>
    12. Setelah klik tombol <b>CheckOut,</b> secara otomatis akan mencetak struk transaksi tersebut <br>
    <p><img src="{{asset('storage/Documentation/pos12.png')}}" alt="pointofsales" style="margin-left: auto; margin-right: auto; display: block; padding: 15px; width: 75%;"></p>
    Berikut ini merupakan contoh struk transaksi <br>
    <p><img src="{{asset('storage/Documentation/pos121.png')}}" alt="pointofsales" style="margin-left: auto; margin-right: auto; display: block; padding: 15px; width: 75%;"></p>
    <u><b>Shipped Transaction:</b></u><br>
    1. Pilih produk yang akan di pasarkan <br>
    <p><img src="{{asset('storage/Documentation/shipped1.png')}}" alt="shippedtransaction" style="margin-left: auto; margin-right: auto; display: block; padding: 15px; width: 75%;"></p>
    2. Masukan jumlah produk yang akan di pasarkan
    <p><img src="{{asset('storage/Documentation/shipped2.png')}}" alt="shippedtransaction" style="margin-left: auto; margin-right: auto; display: block; padding: 15px; width: 75%;"></p>
    3. Pilih <b>Discount</b> yang akan digunakan (Optional) <br>
    <p><img src="{{asset('storage/Documentation/shipped3.png')}}" alt="shippedtransaction" style="margin-left: auto; margin-right: auto; display: block; padding: 15px; width: 75%;"></p>
    4. Klik tombol <b>Purchase</b><br>
    <p><img src="{{asset('storage/Documentation/shipped4.png')}}" alt="shippedtransaction" style="margin-left: auto; margin-right: auto; display: block; padding: 15px; width: 75%;"></p>
    5. Pilih <b>Transaction Type/Tipe Transaksi</b> yang akan digunakan <b>Shipped Transaction</b><br>
    <p><img src="{{asset('storage/Documentation/shipped5.png')}}" alt="shippedtransaction" style="margin-left: auto; margin-right: auto; display: block; padding: 15px; width: 75%;"></p>
    6. Pilih <b>Discount</b> yang akan digunakan (Optional) <br>
    <p><img src="{{asset('storage/Documentation/shipped6.png')}}" alt="shippedtransaction" style="margin-left: auto; margin-right: auto; display: block; padding: 15px; width: 75%;"></p>
    7. Masukan jumlah <b>Discount</b> yang diberikan <br>
    <p><img src="{{asset('storage/Documentation/shipped7.png')}}" alt="shippedtransaction" style="margin-left: auto; margin-right: auto; display: block; padding: 15px; width: 75%;"></p>
    8. Pilih <b>Member,</b> apabila memiliki <b>Member Card</b> (Optional) <br>
    <p><img src="{{asset('storage/Documentation/shipped8.png')}}" alt="shippedtransaction" style="margin-left: auto; margin-right: auto; display: block; padding: 15px; width: 75%;"></p>
    9. Lengkapi <b>Shipping Address/Alamat Pengiriman.</b> Pilih <b>Province/Provinsi</b> yang akan ditujukan sebagai alamat pengiriman. <br>
    <p><img src="{{asset('storage/Documentation/shipped9.png')}}" alt="shippedtransaction" style="margin-left: auto; margin-right: auto; display: block; padding: 15px; width: 75%;"></p>
    10. Pilih <b>City/Kota</b> yang akan digunakan sebagai alamat pengiriman <br>
    <p><img src="{{asset('storage/Documentation/shipped10.png')}}" alt="shippedtransaction" style="margin-left: auto; margin-right: auto; display: block; padding: 15px; width: 75%;"></p>
    11. Pilih <b>District/Kecamatan</b> yang akan digunakan sebagai alamat pengiriman <br>
    <p><img src="{{asset('storage/Documentation/shipped11.png')}}" alt="shippedtransaction" style="margin-left: auto; margin-right: auto; display: block; padding: 15px; width: 75%;"></p>
    12. Isi <b>Address/Alamat</b> yang akan digunakan sebagai alamat pengiriman <br>
    <p><img src="{{asset('storage/Documentation/shipped12.png')}}" alt="shippedtransaction" style="margin-left: auto; margin-right: auto; display: block; padding: 15px; width: 75%;"></p>
    13. Masukan <b>Postal Code/Kode Pos</b><br>
    <p><img src="{{asset('storage/Documentation/shipped13.png')}}" alt="shippedtransaction" style="margin-left: auto; margin-right: auto; display: block; padding: 15px; width: 75%;"></p>
    14. Masukan <b>Phone/Nomor Telepon/Nomor Handphone</b><br>
    <p><img src="{{asset('storage/Documentation/shipped14.png')}}" alt="shippedtransaction" style="margin-left: auto; margin-right: auto; display: block; padding: 15px; width: 75%;"></p>
    15. Pilih <b>Cargo</b>, jasa pengiriman barang yang akan digunakan untuk mengirimkan barang <br>
    <p><img src="{{asset('storage/Documentation/shipped15.png')}}" alt="shippedtransaction" style="margin-left: auto; margin-right: auto; display: block; padding: 15px; width: 75%;"></p>
    16. Pilih <b>Service Type</b><br>
    <p><img src="{{asset('storage/Documentation/shipped16.png')}}" alt="shippedtransaction" style="margin-left: auto; margin-right: auto; display: block; padding: 15px; width: 75%;"></p>
    17. Apabila telah memilih <b>Cargo</b> yang akan digunakan, maka secara otomatis <b>Price Shipping/Harga Pengiriman</b> akan tampil, agar <b>Price Shipping/Biaya Pengiriman</b> tampil, terlebih dahulu lengkapi alamat dan pilih cargo maka <b>Price Shipping/Biaya Pengiriman</b> akan muncul. <br>
    <p><img src="{{asset('storage/Documentation/shipped17.png')}}" alt="shippedtransaction" style="margin-left: auto; margin-right: auto; display: block; padding: 15px; width: 75%;"></p>
    18. Pilih <b>Payment Method</b> yang akan digunakan <br>
    <p><img src="{{asset('storage/Documentation/shipped18.png')}}" alt="shippedtransaction" style="margin-left: auto; margin-right: auto; display: block; padding: 15px; width: 75%;"></p>
    19. <b>Note</b> (Optional) <br>
    <p><img src="{{asset('storage/Documentation/shipped19.png')}}" alt="shippedtransaction" style="margin-left: auto; margin-right: auto; display: block; padding: 15px; width: 75%;"></p>
    20. Klik tombol <b>CheckOut</b><br>
    <p><img src="{{asset('storage/Documentation/shipped20.png')}}" alt="shippedtransaction" style="margin-left: auto; margin-right: auto; display: block; padding: 15px; width: 75%;"></p>
    21. Maka secara otomatis struk transaksi akan tercetak <br>
    <p><img src="{{asset('storage/Documentation/shipped21.png')}}" alt="shippedtransaction" style="margin-left: auto; margin-right: auto; display: block; padding: 15px; width: 75%;"></p>
    Berikut ini merupakan contoh struk yang akan tercetak secara otomatis <br>
    <p><img src="{{asset('storage/Documentation/shipped211.png')}}" alt="shippedtransaction" style="margin-left: auto; margin-right: auto; display: block; padding: 15px; width: 75%;"></p>
    22. Apabila telah melakukan <b>POS (Point Of Sales)</b>, maka secara otomatis akan tersimpan di <b>History > List History</b> baik yang <b>Direct Transaction/Shipped Transaction</b> <br>
    <p><img src="{{asset('storage/Documentation/shipped22.png')}}" alt="shippedtransaction" style="margin-left: auto; margin-right: auto; display: block; padding: 15px; width: 75%;"></p>
  </div>
</div>