<!doctype html>
<html lang="en">
  <head>
    <title>Inventory</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  </head>
  <body>

    <div class="container">
        <h3 style="text-align: center; padding: 15px">Inventory</h3>
        <hr>
        <ul>
            <li><b>Product Listing:</b></li>
            1. Tambahkan terlebih dahulu <b>Product</b> yang akan di perjual-belikan <br>
            <p><img src="{{asset('inventory1.png')}}" alt="inventory" style="margin-left: auto; margin-right: auto; display: block; padding: 15px;"></p>
            2. Masukan <b>Title Product/Judul Produk</b><br>
            <p><img src="{{asset('inventory2.png')}}" alt="inventory" style="margin-left: auto; margin-right: auto; display: block; padding: 15px;"></p>
            3. Pilih <b>Brand</b> yang akan digunakan <br>
            <p><img src="{{asset('inventory3.png')}}" alt="inventory" style="margin-left: auto; margin-right: auto; display: block; padding: 15px;"></p>
            4. Pilih <b>Gender Product</b> yang akan di pasarkan
            <p><img src="{{asset('inventory4.png')}}" alt="inventory" style="margin-left: auto; margin-right: auto; display: block; padding: 15px;"></p>
            5. Pilih <b>Category Product</b><br>
            <p><img src="{{asset('inventory5.png')}}" alt="inventory" style="margin-left: auto; margin-right: auto; display: block; padding: 15px;"></p>
            6. Pilih <b>Sub-Category Product</b><br>
            <p><img src="{{asset('inventory6.png')}}" alt="inventory" style="margin-left: auto; margin-right: auto; display: block; padding: 15px;"></p>
            7. Isi <b>Description/Deskripsi</b> (Optional)<br>
            <p><img src="{{asset('inventory7.png')}}" alt="inventory" style="margin-left: auto; margin-right: auto; display: block; padding: 15px;"></p>
            8. Pilih <b>Gambar</b> yang akan digunakan <br>
            <p><img src="{{asset('inventory8.png')}}" alt="inventory" style="margin-left: auto; margin-right: auto; display: block; padding: 15px;"></p>
            9. Klik tombol <b>Simpan,</b> secara otomatis akan tersimpan dan tampil di <b>List Product</b>. Untuk melihat deskripsinya klik tombol <b>+</b><br>
            <p><img src="{{asset('inventory9.png')}}" alt="inventory" style="margin-left: auto; margin-right: auto; display: block; padding: 15px;"></p>
            <p><img src="{{asset('inventory91.png')}}" alt="inventory" style="margin-left: auto; margin-right: auto; display: block; padding: 15px;"></p>
            10. Setelah klik tombol <b>Simpan,</b> langkah berikutnya <b>Add Item Varian To Product/Tambah Jenis Variasi Barang</b><br>
            <p><img src="{{asset('inventory10.png')}}" alt="inventory" style="margin-left: auto; margin-right: auto; display: block; padding: 15px;"></p>
            11. Pilih <b>Color/Warna</b><br>
            <p><img src="{{asset('inventory11.png')}}" alt="inventory" style="margin-left: auto; margin-right: auto; display: block; padding: 15px;"></p>
            12. Pilh <b>Size/Ukuran</b><br>
            <p><img src="{{asset('inventory12.png')}}" alt="inventory" style="margin-left: auto; margin-right: auto; display: block; padding: 15px;"></p>
            13. Masukan <b>Base Price/Harga Dasar</b><br>
            <p><img src="{{asset('inventory13.png')}}" alt="inventory" style="margin-left: auto; margin-right: auto; display: block; padding: 15px;"></p>
            14. Masukan <b>Sell Price/Harga Jual</b><br>
            <p><img src="{{asset('inventory14.png')}}" alt="inventory" style="margin-left: auto; margin-right: auto; display: block; padding: 15px;"></p>
            15. Masuk jumlah <b>Weight/Berat</b><br>
            <p><img src="{{asset('inventory15.png')}}" alt="inventory" style="margin-left: auto; margin-right: auto; display: block; padding: 15px;"></p>
            16. Pilih <b>Weight Unit/Satuan Berat</b> dari produk <br>
            <p><img src="{{asset('inventory16.png')}}" alt="inventory" style="margin-left: auto; margin-right: auto; display: block; padding: 15px;"></p>
            17. Pilih <b>Condition/Kondisi</b> dari produk <br>
            <p><img src="{{asset('inventory17.png')}}" alt="inventory" style="margin-left: auto; margin-right: auto; display: block; padding: 15px;"></p>
            18. Pilih <b>Type/Type Product</b><br>
            <p><img src="{{asset('inventory18.png')}}" alt="inventory" style="margin-left: auto; margin-right: auto; display: block; padding: 15px;"></p>
            19. Pilih <b>Image Variant/Variasi Gambar</b><br>
            <p><img src="{{asset('inventory19.png')}}" alt="inventory" style="margin-left: auto; margin-right: auto; display: block; padding: 15px;"></p>
            20. Klik tombol <b>Simpan</b><br>
            <p><img src="{{asset('inventory20.png')}}" alt="inventory" style="margin-left: auto; margin-right: auto; display: block; padding: 15px;"></p>
            21. Setelah <b>Add Item Variant To Product/Tambah Jenis Variasi Barang</b>, secara otomatis tersimpan dan tampil di tabel list dibawah ini. Untuk melihat lebih rinci klik tombol <b>+</b>
            <p><img src="{{asset('inventory21.png')}}" alt="brands" style="margin-left: auto; margin-right: auto; display: block; padding: 15px;"></p>
            <li><b>Product Order</b></li>
            1. Tambahkan <b>Create Order</b> terlebih dahulu <br>
            <p><img src="{{asset('productorder1.png')}}" alt="productorder" style="margin-left: auto; margin-right: auto; display: block; padding: 15px;"></p>
            2. Kemudian pilih tanggal <b>Purchase Order/PO</b> yang telah tersimpan pada <b>List Purchase Order</b><br>
            <p><img src="{{asset('productorder2.png')}}" alt="productorder" style="margin-left: auto; margin-right: auto; display: block; padding: 15px;"></p>
            3. Pilih <b>Vendor</b> yang akan digunakan <br>
            <p><img src="{{asset('productorder3.png')}}" alt="productorder" style="margin-left: auto; margin-right: auto; display: block; padding: 15px;"></p>
            4. Isi <b>Note/Catatan</b> (Optional) <br>
            <p><img src="{{asset('productorder4.png')}}" alt="productorder" style="margin-left: auto; margin-right: auto; display: block; padding: 15px;"></p>
            5. Klik tombol <b>SAVE</b><br>
            <p><img src="{{asset('productorder5.png')}}" alt="productorder" style="margin-left: auto; margin-right: auto; display: block; padding: 15px;"></p>
            6. Setelah menambahkan <b>Create Order</b>, maka akan secara otomatis tersimpan dan tampil di <b>List Order</b><br>
            <p><img src="{{asset('productorder6.png')}}" alt="productorder" style="margin-left: auto; margin-right: auto; display: block; padding: 15px;"></p>
            7. Masukan <b>Quantity/Kuantitas</b> barang yang akan di pesan <br>
            <p><img src="{{asset('productorder7.png')}}" alt="productorder" style="margin-left: auto; margin-right: auto; display: block; padding: 15px;"></p>
            8. Klik tombol <b>SAVE</b> <br>
            <p><img src="{{asset('productorder8.png')}}" alt="productorder" style="margin-left: auto; margin-right: auto; display: block; padding: 15px;"></p>
            9. Setelah <b>Add Item Purchase Order</b>, secara otomatis akan tersimpan dan tampil pada tabel seperti pada gambar berikut di bawah ini <br>
            <p><img src="{{asset('productorder9.png')}}" alt="productorder" style="margin-left: auto; margin-right: auto; display: block; padding: 15px;"></p>
            10. Setelah selesai kemudian klik tombol <b>New Order</b><br>
            <p><img src="{{asset('productorder10.png')}}" alt="productorder" style="margin-left: auto; margin-right: auto; display: block; padding: 15px;"></p>
            11. Setelah klik tombol <b>New Order</b> status order akan berubah secara otomatis <br>
            <p><img src="{{asset('productorder11.png')}}" alt="productorder" style="margin-left: auto; margin-right: auto; display: block; padding: 15px;"></p>
            12. Klik tombol <b>Approve</b><br>
            <p><img src="{{asset('productorder12.png')}}" alt="productorder" style="margin-left: auto; margin-right: auto; display: block; padding: 15px;"></p>
            13. Setelah klik tombol <b>Approve</b>, maka secara otomatis status <b>Order</b> akan berubah
            <p><img src="{{asset('productorder13.png')}}" alt="productorder" style="margin-left: auto; margin-right: auto; display: block; padding: 15px;"></p>
            <li><b>Product Receipt:</b></li>
            1. Pilih <b>Date/Tanggal</b> ketika melakukan <b>Purchase Order</b><br>
            <p><img src="{{asset('productreceipt1.png')}}" alt="productreceipt" style="margin-left: auto; margin-right: auto; display: block; padding: 15px;"></p>
            2. Kemudian pilih <b>Purchase Order/PO</b> yang telah tersimpan pada <b>List Purchase Order</b><br>
            <p><img src="{{asset('productreceipt2.png')}}" alt="productreceipt" style="margin-left: auto; margin-right: auto; display: block; padding: 15px;"></p>
            3. <b>Note</b> boleh di isi atau tidak di isi tidak apa-apa (Optional) <br>
            <p><img src="{{asset('productreceipt3.png')}}" alt="productreceipt" style="margin-left: auto; margin-right: auto; display: block; padding: 15px;"></p>
            4. Klik tombol <b>SAVE</b>
            <p><img src="{{asset('productreceipt4.png')}}" alt="productreceipt" style="margin-left: auto; margin-right: auto; display: block; padding: 15px;"></p>
            5. <b>Purchase Order</b> yang sebelumnya telah tersimpan secara otomatis datanya terdapat di <b>Product Receipt</b><br>
            <p><img src="{{asset('productreceipt5.png')}}" alt="productreceipt" style="margin-left: auto; margin-right: auto; display: block; padding: 15px;"></p>
            6. Klik tombol <b>New Receipt</b><br>
            <p><img src="{{asset('productreceipt6.png')}}" alt="productreceipt" style="margin-left: auto; margin-right: auto; display: block; padding: 15px;"></p>
            7. Setelah klik tombol <b>New Receipt</b>, secara otomatis status <b>Receipt</b> berubah <br>
            <p><img src="{{asset('productreceipt7.png')}}" alt="productreceipt" style="margin-left: auto; margin-right: auto; display: block; padding: 15px;"></p>
            8. Klik tombol <b>Approve</b>
            <p><img src="{{asset('productreceipt8.png')}}" alt="productreceipt" style="margin-left: auto; margin-right: auto; display: block; padding: 15px;"></p>
            9. Setelah klik tombol <b>Approve</b>, maka status <b>Receipt</b> berubah <br>
            <p><img src="{{asset('productreceipt9.png')}}" alt="productreceipt" style="margin-left: auto; margin-right: auto; display: block; padding: 15px;"></p>
            10. Klik tombol <b>Approve</b><br>
            <p><img src="{{asset('productreceipt10.png')}}" alt="productreceipt" style="margin-left: auto; margin-right: auto; display: block; padding: 15px;"></p>
            11. Setelah klik tombol <b>Approve</b>, secara otomatis status <b>Receipt</b> berubah <br>
            <p><img src="{{asset('productreceipt11.png')}}" alt="productreceipt" style="margin-left: auto; margin-right: auto; display: block; padding: 15px;"></p>
            12. <b>Status Order & Receipt</b> di <b>Purchase Order</b>-pun akan berubah secara otomatis
            <p><img src="{{asset('productreceipt12.png')}}" alt="productreceipt" style="margin-left: auto; margin-right: auto; display: block; padding: 15px;"></p>
        </ul>
    </div>
      
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  </body>
</html>