<div class="panel panel-primary">
  <div class="panel-body" style="text-align: justify; word-spacing: 5px;">
    1. Tambahkan terlebih dahulu <b>Vendor</b> yang akan digunakan
    <p><img src="{{asset('storage/Documentation/v1.png')}}" alt="vendor" style="margin-left: auto; margin-right: auto; display: block; padding: 15px; width: 75%;"></p>
    2. Masukan <b>Name/Nama Vendor</b>
    <p><img src="{{asset('storage/Documentation/v2.png')}}" alt="vendor" style="margin-left: auto; margin-right: auto; display: block; padding: 15px; width: 75%;"></p>
    3. Masukan nama <b>PIC (Person In Charge)/Penanggung Jawab Vendor</b>
    <p><img src="{{asset('storage/Documentation/v3.png')}}" alt="vendor" style="margin-left: auto; margin-right: auto; display: block; padding: 15px; width: 75%;"></p>
    4. Masukan alamat email <b>PIC</b>
    <p><img src="{{asset('storage/Documentation/v4.png')}}" alt="vendor" style="margin-left: auto; margin-right: auto; display: block; padding: 15px; width: 75%;"></p>
    5. Pilih <b>Province/Provinsi Vendor</b>
    <p><img src="{{asset('storage/Documentation/v5.png')}}" alt="vendor" style="margin-left: auto; margin-right: auto; display: block; padding: 15px; width: 75%;"></p>
    6. Pilih <b>City/Kota Vendor</b>
    <p><img src="{{asset('storage/Documentation/v6.png')}}" alt="vendor" style="margin-left: auto; margin-right: auto; display: block; padding: 15px; width: 75%;"></p>
    7. Pilih <b>District/Kecamatan Vendor</b>
    <p><img src="{{asset('storage/Documentation/v7.png')}}" alt="vendor" style="margin-left: auto; margin-right: auto; display: block; padding: 15px; width: 75%;"></p>
    8. Pilih <b>Address/Alamat Vendor</b>
    <p><img src="{{asset('storage/Documentation/v8.png')}}" alt="vendor" style="margin-left: auto; margin-right: auto; display: block; padding: 15px; width: 75%;"></p>
    9. Masukan <b>Phone/Nomor Telepon Vendor</b>
    <p><img src="{{asset('storage/Documentation/v9.png')}}" alt="vendor" style="margin-left: auto; margin-right: auto; display: block; padding: 15px; width: 75%;"></p>
    10. Klik tombol <b>SAVE</b>
    <p><img src="{{asset('storage/Documentation/v10.png')}}" alt="vendor" style="margin-left: auto; margin-right: auto; display: block; padding: 15px; width: 75%;"></p>
    11. <b>Vendor</b> yang telah di tambahkan, secara otomatis tersimpan dan tampil di <b>List Store/Daftar Store</b>
    <p><img src="{{asset('storage/Documentation/v11.png')}}" alt="vendor" style="margin-left: auto; margin-right: auto; display: block; padding: 15px; width: 75%;"></p>
  </div>
</div>

<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>

</html>