<?php

namespace App\Http\Controllers\Documentation;

use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use View;

class productlisting_controller extends Controller
{
    public function index() {
        $this->grid = View::make('Documentation/ProductListing');

        return Admin::content(function (Content $content) {

            $content->header('Product');
            $content->description(' ');
            $content->body($this->grid);
        });
    }
}
