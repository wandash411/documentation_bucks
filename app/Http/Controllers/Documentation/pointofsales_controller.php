<?php

namespace App\Http\Controllers\Documentation;

use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use View;

class pointofsales_controller extends Controller
{
    public function index() {
        $this->grid = View::make('Documentation/PointOfSales');

        return Admin::content(function (Content $content) {

            $content->header('Point Of Sales');
            $content->description(' ');
            $content->body($this->grid);
        });
    }
}
