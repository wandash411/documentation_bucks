<?php

namespace App\Http\Controllers\Documentation;

use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use View;

class guide_controller extends Controller
{
    public function index() {
        $this->grid = View::make('Documentation/Guide');

        return Admin::content(function (Content $content) {

            $content->header('Guide');
            $content->description(' ');
            $content->body($this->grid);
        });
    }
}
