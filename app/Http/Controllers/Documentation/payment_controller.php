<?php

namespace App\Http\Controllers\Documentation;

use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use View;

class payment_controller extends Controller
{
    public function index() {
        $this->grid = View::make('Documentation/Payment');

        return Admin::content(function (Content $content) {

            $content->header('Payment Method');
            $content->description(' ');
            $content->body($this->grid);
        });
    }
}
