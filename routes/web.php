<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('welcome');
});
Route::group([
    'prefix'     => 'admin',
    'middleware' => ['web', 'admin'],
], function () {
    Route::group([
        'prefix'     => 'documentation',
        'namespace'  => 'Documentation',
    ], function () {
        Route::get('/payment', 'payment_controller@index');
        Route::get('/preferences', 'preferences_controller@index');
        Route::get('/inventory', 'inventory_controller@index');
        Route::get('/sales', 'sales_controller@index');
        Route::get('/overview', 'gettingstarted_controller@index');
        Route::get('/analytic', 'analytic_controller@index');
        Route::get('/productlisting', 'productlisting_controller@index');
        Route::get('/purchaseorder', 'purchaseorder_controller@index');
        Route::get('/vendor', 'vendor_controller@index');
        Route::get('/productreceipt', 'productreceipt_controller@index');
        Route::get('/pointofsales', 'pointofsales_controller@index');
        Route::get('/history', 'history_controller@index');
        Route::get('/guide', 'guide_controller@index');
    });
});
